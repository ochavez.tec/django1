from django.urls import path
from todolist import views

urlpatterns = [
    path('', views.index),
    path('holamundo/', views.holamundo),
    path('tasks.html', views.task),
    path('activity.html', views.activity),
    path('crear_tarea.html', views.create_task),
    path('create_activity.html', views.create_activity),
    path('task_list.html', views.tarea_list.as_view()),
    
    
]