from django import forms
from .models import tarea
class CrearTarea(forms.Form):
    texto = forms.CharField(label="Nombre de la tarea", max_length=200)
    completada = forms.BooleanField(required=False, initial=True)

class CrearActividad(forms.Form):
    actividad = forms.CharField(label="Nombre de la actividad", max_length=200)
    tarea = forms.ModelChoiceField(queryset=tarea.objects.all())
