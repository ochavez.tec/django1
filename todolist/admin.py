from django.contrib import admin

from .models import actividad, tarea

admin.site.register(tarea)
admin.site.register(actividad)
