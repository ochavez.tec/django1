from django.shortcuts import render
from django.http import HttpResponse

# Create your views here.
def holapp2(request):
    return HttpResponse("<h1>Hola Django App2</h1>")

def index2(request):
    return render(request, 'index2.html')