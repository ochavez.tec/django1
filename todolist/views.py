from django.shortcuts import render, redirect
from django.http import HttpResponse
from .models import actividad, tarea
from .forms import CrearTarea, CrearActividad
from django.views.generic import ListView, CreateView

# Create your views here.

def index(request):
    msj = "Este es un parametro de la clase del 19"
    return render(request, 'index.html', {
        'mensaje': msj
    })

def holamundo(request):
    return HttpResponse("<h1>Hola Django World</h1>")

def task(request):
    tareas = tarea.objects.all()
    return render(request, 'tasks.html', {
        'tasks': tareas
    })

def activity(request):
    actividades = actividad.objects.all()
    return render(request, 'activity.html', {
        'activities': actividades
    })

def create_task(request):
    if request.method == 'GET':
        return render(request, 'crear_tarea.html', {
        'form': CrearTarea(),
    })
    else:
        text = request.POST['texto']
        if 'completada' in request.POST:
            completada = True
        else:
            completada = False
        tarea.objects.create(texto = text, completado=completada)
        return redirect('crear_tarea.html')

def create_activity(request):
    if request.method == 'GET':
        return render(request, 'create_activity.html', {
        'form': CrearActividad(),
    })
    else:
        activity = request.POST['actividad']
        idTarea = request.POST['tarea']
        task = tarea.objects.get(id=idTarea)
        actividad.objects.create(actividad=activity, tarea = task)
        return redirect('create_activity.html')


class tarea_list(ListView):
    model = tarea
    template_name = 'task_list.html'

