from django.db import models

# Create your models here.
class tarea(models.Model):
    texto = models.CharField(max_length=200)
    completado = models.BooleanField(default=False)

    def __str__(self) -> str:
        return self.texto

class actividad(models.Model):
    actividad = models.CharField(max_length=200)
    tarea = models.ForeignKey(tarea, on_delete=models.CASCADE)

    def __str__(self) -> str:
        return self.actividad